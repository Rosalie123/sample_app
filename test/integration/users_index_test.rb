require 'test_helper'

class UsersIndexTest < ActionDispatch::IntegrationTest

  def setup
    @admin = users(:alan)
    @non_admin = users(:ada)
    @non_activated_user = users(:willy)
  end

  test "Admin user index showing users properly with paginate" do
    log_in_as(@admin)
    get users_path
    assert_template "users/index"
    assert_select 'div.pagination', count: 2
    User.where(activated: true).paginate(page: 1).each do |user|
      assert_select "a[href=?]", user_path(user), text: user.name
      assert_select "a", text: @non_activated_user.name, count: 0
      unless user == @admin
        assert_select "a[href=?]", user_path(user), text: 'delete'
      end
    end

    assert_difference 'User.count', -1 do
      delete user_path(@non_admin)
    end
  end

  test "index as non_admin" do
    log_in_as(@non_admin)
    get users_path
    assert_select "a", text: 'delete', count: 0
    assert_select "a", text: @non_activated_user.name, count: 0
  end

  test "shouldn't show non_activated user" do
    log_in_as(@non_activated_user)
    get user_path(@non_activated_user)
    assert_redirected_to root_url
  end





end
