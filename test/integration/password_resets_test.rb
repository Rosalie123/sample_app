require 'test_helper'

class PasswordResetsTest < ActionDispatch::IntegrationTest

  def setup
    ActionMailer::Base.deliveries.clear
    @user = users(:alan)
  end

  test "invalid email submission reset password" do
    get new_password_reset_path
    assert_template 'password_resets/new'
    post password_resets_path, params: { password_reset: { email: "" }}
    assert_equal 0, ActionMailer::Base.deliveries.size
    assert_not flash.empty?
    assert_template 'password_resets/new'
  end

  test "valid email submission reset password" do
    get new_password_reset_path
    assert_template 'password_resets/new'
    post password_resets_path, params: { password_reset: { email: @user.email }}
    # ** checking reset_digest well loaded in db **
    assert_not_equal @user.reset_digest, @user.reload.reset_digest
    assert_equal 1, ActionMailer::Base.deliveries.size
    assert_not flash.empty?
    assert_redirected_to root_url
    # get access user from controller
    user = assigns(:user)

    # valid email but invalid token
    get edit_password_reset_path("wrong token", email: user.email)
    assert_redirected_to root_url

    # valid token but invalid email
    get edit_password_reset_path(user.reset_token, email: "")
    assert_redirected_to root_url

    # invalid user
    user.toggle!(:activated)
    get edit_password_reset_path(user.reset_token, email: user.email)
    assert_redirected_to root_url
    user.toggle!(:activated)

    # valid token and valid email
    get edit_password_reset_path(user.reset_token, email: user.email)
    assert_template 'password_resets/edit'
    assert_select "input[name=email][type=hidden][value=?]", user.email

    # empty password
    patch password_reset_path(user.reset_token),
          params: { email: user.email,
                    user: { password: "",
                            password_confirmation: "" }}
    assert_select 'div#error_explanation'
    assert_template 'password_resets/edit'
    assert_equal user.password_digest, user.reload.password_digest

    # invalid password & confirmation
    patch password_reset_path(user.reset_token),
          params: { email: user.email,
                    user: { password: "foo",
                            password_confirmation: "bar" }}
    assert_select 'div#error_explanation'
    assert_select 'li.error', count: 2
    assert_template 'password_resets/edit'
    assert_equal user.password_digest, user.reload.password_digest

    # valid password & confirmation
    patch password_reset_path(user.reset_token),
        params:  { email: user.email,
                   user: { password: "password",
                           password_confirmation: "password"}}
    assert is_logged_in?
    assert_not flash.empty?
    assert_not_equal user.password_digest, user.reload.password_digest
    assert_nil user.reload.reset_digest
    assert_redirected_to user
  end

  test "expired_token" do
    get new_password_reset_path
    post password_resets_path,
         params: { password_reset: { email: @user.email}}
    @user = assigns(:user)
    @user.update_attribute(:reset_sent_at, 3.hours.ago)

    # check password reset page results in redirect and contains expired
    get edit_password_reset_path(@user.reset_token, email: @user.email)
    assert_response :redirect
    follow_redirect!
    assert_match /expired/i, response.body

    # check update results in redirect and contains expired
    patch password_reset_path(@user.reset_token),
          params: { email: @user.email,
                    user: { password: "foobar",
                            password_confirmation: "foobar" }}
    assert_response :redirect
    follow_redirect!
    assert_match /expired/i, response.body
  end




end
