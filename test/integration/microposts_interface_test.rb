require 'test_helper'

class MicropostsInterfaceTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:alan)
  end

  test "micropost sidebar count" do
    log_in_as(@user)
    get root_path
    assert_match "#{@user.microposts.count} microposts", response.body

    # User with zero micropost
    other_user = users(:nee)
    log_in_as(other_user)
    get root_path
    assert_match "0 microposts", response.body
    other_user.microposts.create!(content: "hello word")
    get root_path
    assert_match "#{other_user.microposts.count} micropost", response.body


  end

  test "micropost interface" do
    log_in_as @user
    get root_path
    assert_select 'div.pagination'
    assert_select 'input[type=file]'
    assert_match @user.microposts.count.to_s, response.body
    @user.feed.paginate(page: 1).each do |micropost|
      assert_match micropost.content, response.body
    end
    # Invalid submission
    assert_no_difference 'Micropost.count' do
      post microposts_path, params: { micropost: { content: ""}}
    end
    assert_template 'static_pages/home'
    assert_select 'div#error_explanation'

    # Valid submission
    content = "Hello world"
    picture = fixture_file_upload('test/fixtures/rails.png','image/png')
    assert_difference 'Micropost.count', 1 do
      post microposts_path, params: { micropost: { content: content,
                                                   picture: picture } }
    end
    assert_redirected_to root_url
    assert assigns(:micropost).picture?
    follow_redirect!
    assert_match content, response.body

    # destroy a post
    assert_select 'a', text: 'delete'
    micropost = @user.microposts.paginate(page: 1).first
    assert_difference 'Micropost.count', -1 do
      delete micropost_path(micropost)
    end

    # visit different user (no delete links)
    get user_path(users(:ada))
    assert_select 'a', text: 'delete', count:0

    get user_path(@user)
    assert_select 'a', text: 'delete'
  end

end
