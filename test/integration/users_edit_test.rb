require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:alan)
  end

  test "user update with invalid information" do
   log_in_as(@user)
   get edit_user_path(@user)
   assert_template "users/edit"
   patch edit_user_path(@user), params: { user: { name: "",
                                                  password: "foo",
                                                  email: "",
                                                  password_confirmation: "bar" } }
   assert_template "users/edit"
  end


  test "user edit successlly with friendly forwarding" do
    get edit_user_path(@user)
    log_in_as(@user)
    assert_redirected_to edit_user_path(@user)
    follow_redirect!
    assert_template 'users/edit'
    name = "Foo Bar"
    email = "foo@bar.com"
    patch edit_user_path(@user), params: { user: { name: name,
                                                   email: email,
                                                   password: "",
                                                   password_confirmation: "" } }
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal name, @user.name
    assert_equal email, @user.email
  end

end
