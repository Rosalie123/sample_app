require 'test_helper'

class MicropostsControllerTest < ActionDispatch::IntegrationTest

  def setup
    @micropost = microposts(:orange)
    @other_micropost = microposts(:tau_manifesto)
    @user = users(:alan)
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'Micropost.count' do
      post microposts_path, params: { micropost: { content: "Lorem ipsum"}}
    end
      assert_redirected_to login_path
  end

  test "should redirect distroy when not logged in" do
    assert_no_difference 'Micropost.count' do
      delete micropost_path(@micropost)
    end
    assert_redirected_to login_path
  end

  test "current user can't delete other people's micropost" do
    log_in_as @user
    assert_no_difference 'Micropost.count' do
      delete micropost_path(@other_micropost)
    end
    assert_redirected_to root_url
  end

end
